Documentation for next_view_entity_reference_example

This module provides:
* an example_article node that uses the Tags taxonomy
* an next_tags_example view 

This module provides an example that uses next_view_entity_reference
to create a block that provide next/prior links based on the URL
and the tags that an example_article instance contains. 

A typical implemenation using this approach would be a 
digital magazine that has articles that are tagged. 
Viewing the a node with a url of the form:

/node/123?tag=1

The block using the supplied view will display the 
next/prior links based on a tag field value of 1. 
The entity reference view display uses the 'tag'
argument. 


Install:
Install and enable as a conventional module


Uninstall:
Remove any blocks created while testing. These cannot
be removed by the module. 

Uninstall as a conventional module. This will remove
the view and example_article content type as well as
all instances of example_article.

It does not affect any blocks you may have added that
use the view.


Use:
Place a Next view entity reference block on the 
page via /admin/structure/block. Set the 
Entity Reference Next View and Block Table of 
Contents View from the provide example views. 

Create one or more Tag taxonomy values. They can 
have any names like Term1, Term2, etc. 

Create a number of example_article nodes and 
fill in the Tag field with one or more of the 
Tag terms. 

View an article by modifying the URL to include
the tag argument changing:
 
   /node/3

to:

   /node/3?tag=11

where the node ID for an example_article is 3
and the term ID for one of the Tags in the node
is 11. If there is no matching tag then the list
of example_articles will be empty. 

If there are multiple articles with the 11 tag 
then they will be presented when clicking on the
next or prior links in the block. The example
is set up to copy the URL argument ?tag=11
to each link. 

If the node has more than one tag value then
you can change the ?tag= to another number. 
This will present navigation to a different
set of nodes that have a matching tag value.



Things to do:
It is possible to experiment with different
filtering and sorting. For example, add a 
weight field to the example_article node
and adjust the sample entity reference view
to take this into account.
