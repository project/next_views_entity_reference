<?php

namespace Drupal\next_views_entity_reference\Plugin\Block;

use Drupal\node\NodeInterface;
use Drupal\views\Views;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\UncacheableDependencyTrait;

/**
 * Provides a 'NextViewsEntityReferenceBlock' block.
 *
 * @Block(
 *  id = "next_views_entity_reference_block",
 *  admin_label = @Translation("Next views entity reference block"),
 * )
 */
class NextViewsEntityReferenceBlock extends BlockBase {

  use UncacheableDependencyTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'next_entity_reference_view' => NULL,
      'next_entity_reference_toc' => NULL,
      'next_entity_reference_prior' => '<b>Prior:</b> @',
      'next_entity_reference_no_prior' => 'First',
      'next_entity_reference_next' => '<b>Next:</b> @',
      'next_entity_reference_no_next' => 'Last',
      'next_entity_reference_view_nothing' => TRUE,
      'next_entity_reference_view_arguments' => TRUE,
      'next_entity_reference_view_style' => FALSE,
      'next_entity_reference_view_class' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $entity_views = [];
    $block_views = [NULL => $this->t('- Select -')];
    foreach (Views::getEnabledViews() as $view_id => $view) {
      foreach ($view->get('display') as $display_id => $display) {
        if ($display['display_plugin'] == 'entity_reference') {
          $entity_views[$view_id . ':' . $display_id] = $view->label() . ':' . $display['display_title'];
        }
        if ($display['display_plugin'] == 'block') {
          $block_views[$view_id . ':' . $display_id] = $view->label() . ':' . $display['display_title'];
        }

      }
    }

    asort($entity_views);

    $form['next_entity_reference_view'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Reference Next View'),
      '#description' => $this->t('This view display provides the list of entities in the next/prior sequence.'),
      '#options' => $entity_views,
      '#default_value' => $this->configuration['next_entity_reference_view'],
      '#required' => TRUE,
      '#weight' => '9',
    ];

    $form['next_entity_reference_toc'] = [
      '#type' => 'select',
      '#title' => $this->t('Block Table of Contents View'),
      '#description' => $this->t('Optional view displayed above the next/prior controls.'),
      '#options' => $block_views,
      '#default_value' => $this->configuration['next_entity_reference_toc'],
      '#weight' => '9',
    ];

    $form['next_entity_reference_view_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Display style'),
      '#description' => $this->t('Select how to handle text, arrows and links.'),
      '#default_value' => $this->configuration['next_entity_reference_view_style'],
      '#options' => [
        'text' => $this->t('Text only'),
        'arrows' => $this->t('Arrows only'),
        'both' => $this->t('Arrows and text'),
      ],
      '#required' => TRUE,
      '#weight' => '10',
    ];

    $form['next_entity_reference_view_nothing'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide if nothing found'),
      '#description' => $this->t('Do not show any controls if there are no view results.'),
      '#default_value' => $this->configuration['next_entity_reference_view_nothing'],
      '#weight' => '10',
    ];

    $form['next_entity_reference_view_arguments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Append current URL arguments to next/prior URLs'),
      '#description' => $this->t('This is useful if arguments are used by the view.'),
      '#default_value' => $this->configuration['next_entity_reference_view_arguments'],
      '#weight' => '10',
    ];

    $form[] = [
      '#type' => 'item',
      '#title' => $this->t('Text notes'),
      '#markup' => $this->t('The text fields below use HTML so be careful with things like &lt;angle brackets&gt;. Use encodings like "&#38gt.".'),
      '#weight' => '11',
    ];

    $form['next_entity_reference_prior'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prior text'),
      '#description' => $this->t("Use this text if a prior link is available. An at-sign (@) will be replaced by the entity's title."),
      '#default_value' => $this->configuration['next_entity_reference_prior'],
      '#weight' => '11',
    ];

    $form['next_entity_reference_no_prior'] = [
      '#type' => 'textfield',
      '#title' => $this->t('No prior text'),
      '#description' => $this->t('Use this text if tere is no prior link.'),
      '#default_value' => $this->configuration['next_entity_reference_no_prior'],
      '#weight' => '11',
    ];

    $form['next_entity_reference_next'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Next text'),
      '#description' => $this->t("Use this text if a next link is availablei. An at-sign (@) will be replaced by the entity's title."),
      '#default_value' => $this->configuration['next_entity_reference_next'],
      '#weight' => '11',
    ];

    $form['next_entity_reference_no_next'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Next text'),
      '#description' => $this->t('Use this text if a no next link is available.'),
      '#default_value' => $this->configuration['next_entity_reference_no_next'],
      '#weight' => '11',
    ];

    $form['next_entity_reference_view_class'] = [
      '#type' => 'textfield',
      '#title' => 'CSS class',
      '#description' => $this->t('Additional CSS class definition'),
      '#default_value' => $this->configuration['next_entity_reference_view_class'],
      '#weight' => '15',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['next_entity_reference_view'] = $form_state->getValue('next_entity_reference_view');
    $this->configuration['next_entity_reference_toc'] = $form_state->getValue('next_entity_reference_toc');
    $this->configuration['next_entity_reference_prior'] = $form_state->getValue('next_entity_reference_prior');
    $this->configuration['next_entity_reference_no_prior'] = $form_state->getValue('next_entity_reference_no_prior');
    $this->configuration['next_entity_reference_next'] = $form_state->getValue('next_entity_reference_next');
    $this->configuration['next_entity_reference_no_next'] = $form_state->getValue('next_entity_reference_no_next');
    $this->configuration['next_entity_reference_view_style'] = $form_state->getValue('next_entity_reference_view_arguments');
    $this->configuration['next_entity_reference_view_nothing'] = $form_state->getValue('next_entity_reference_view_nothing');
    $this->configuration['next_entity_reference_view_style'] = $form_state->getValue('next_entity_reference_view_style');
    $this->configuration['next_entity_reference_view_class'] = $form_state->getValue('next_entity_reference_view_class');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $my_entity = \Drupal::routeMatch()->getParameter('node');
    if (!$my_entity instanceof NodeInterface) {
      return [];
    }
    $my_nid = $my_entity->id();

    [$view_id, $display_id] = explode(":", $this->configuration['next_entity_reference_view'], 2);

    $nid = [];
    $entity = [];
    foreach (views_get_view_result($view_id, $display_id) as $row) {
      $entity[] = $row->_entity;
      $nid[] = $row->_entity->id();
    }

    // Get the URL arguments.
    $index = array_search($my_nid, $nid);
    if (($index === FALSE) && ($this->configuration['next_entity_reference_view_nothing'])) {
      return [];
    }

    if ($_GET && $this->configuration['next_entity_reference_view_arguments']) {
      foreach ($_GET as $key => $value) {
        $args[] = $key . '=' . $value;
      }
      $arguments = '?' . implode('&', $args);
    }
    else {
      $arguments = '';
    }

    // Build the form.
    $build['#theme'] = 'next_entity_reference_view_block';
    $build['#content'] = [
      'toc' => $this->configuration['next_entity_reference_toc'],
      'class' => 'next-entity-reference-view-block ' .
      $this->configuration['next_entity_reference_view_class'],
      'style' => $this->configuration['next_entity_reference_view_style'],
      'prior' => ($index == 0) ?
         [
           'url' => '',
           'prompt' => $this->t($this->configuration['next_entity_reference_no_prior']),
           'class' => 'next-entity-reference-view-prior next-entity-reference-view-no-link',
         ] :
         [
           'url' => $entity[$index - 1]->toUrl()->toString() . $arguments,
           'prompt' => $this->t(
              $this->configuration['next_entity_reference_prior'],
              ['@' => $entity[$index - 1]->getTitle()]),
           'title' => $entity[$index - 1]->getTitle(),
           'class' => 'next-entity-reference-view-prior next-entity-reference-view-link',
         ],
      'next' => (isset($entity[$index + 1])) ?
         [
           'url' => $entity[$index + 1]->toUrl()->toString() . $arguments,
           'prompt' => $this->t(
              $this->configuration['next_entity_reference_next'],
              ['@' => $entity[$index + 1]->getTitle()]),
           'title' => $entity[$index + 1]->getTitle(),
           'class' => 'next-entity-reference-view-next next-entity-reference-view-link',
         ] :
         [
           'url' => '',
           'prompt' => $this->t($this->configuration['next_entity_reference_no_next']),
           'class' => 'next-entity-reference-view-next next-entity-reference-view-no-link',
         ],
    ];

    return $build;
  }

}
